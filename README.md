For config state, edit file: 

    pillar/openresty_config.sls

For run state:
    
    sudo salt -v '*' state.apply salt.openresty_install
    sudo salt -v '*' state.apply salt.openresty_setup