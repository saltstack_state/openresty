
add_openresty_repo:
  pkgrepo.managed:
    - name: deb http://openresty.org/package/ubuntu bionic main
    - key_url: https://openresty.org/package/pubkey.gpg
    - failhard: True

{% for pkg in pillar['pkg_for_install_openresty'] %}
{{ pkg }}_install:
  pkg.installed:
    - name: {{ pkg }}
    - require:
        - pkgrepo: add_openresty_repo
{% endfor %}

start_openresty:
  service.running:
    - name: openresty
    - enable: True
    - full_restart: True
    - watch:
        {% for pkg in pillar['pkg_for_install_openresty'] %}
        - pkg: openresty_install
        {% endfor %}

{% for ext_lua in pillar['luarocks_pkg'] %}
luarocks_install_{{ ext_lua }}:
  cmd.run:
    - name: luarocks install {{ ext_lua }}
    - require:
        - service: start_openresty
{% endfor %}