pkg_for_install_openresty:
  - openresty
  - openresty-opm
  - lua5.1
  - lua5.2
  - luarocks
  - libmagickwand-dev
  - imagemagick
  - libmaxminddb-dev

luarocks_pkg:
  - magick
  - md5